import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MachineTest {

    private static final int COOPERATE_MOVE = 1;
    private static final int CHEAT_MOVE = 0;

    @Test
    public void shouldReturnPositive3TokensWhenTheMoveIsCooperative() {
        Machine machine = new Machine();

        int score = machine.calculateScore(COOPERATE_MOVE);

        assertEquals(3, score);
    }

    @Test
    public void shouldNotReturnAnyTokensForACheatMove() {
        Machine machine = new Machine();

        int score = machine.calculateScore(CHEAT_MOVE);

        assertEquals(0, score);
    }
}
