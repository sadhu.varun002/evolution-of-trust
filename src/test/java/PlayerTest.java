import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.util.Scanner;

import static junit.framework.TestCase.assertEquals;

public class PlayerTest {
    @Test
    public void shouldMakeAMoveByCooperating() throws Exception {
        System.setIn(new BufferedInputStream(new ByteArrayInputStream("1".getBytes())));
        Scanner scanner = new Scanner(System.in);
        Player player = new Player(scanner);

        int result = player.move();

        assertEquals(1, result);
    }

    @Test
    public void shouldMakeAMoveByCheating() throws Exception {
        System.setIn(new BufferedInputStream(new ByteArrayInputStream("0".getBytes())));
        Scanner scanner = new Scanner(System.in);
        Player player = new Player(scanner);

        int result = player.move();

        assertEquals(0, result);
    }

    @Test(expected = Exception.class)
    public void shouldThrowAnInvalidInputExceptionWhenTheMoveIsNotValid() throws Exception {
        System.setIn(new BufferedInputStream(new ByteArrayInputStream("4".getBytes())));
        Scanner scanner = new Scanner(System.in);
        Player player = new Player(scanner);

        player.move();
    }
}
