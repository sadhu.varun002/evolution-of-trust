import java.util.Scanner;

public class Player {

    private final Scanner console;

    public Player(Scanner console) {
        this.console = console;
    }

    public int move() throws Exception {
        int move = this.console.nextInt();
        if (move == 1 || move == 0) {
            return move;
        }

        throw new Exception("Invalid move. Please enter either 1 or 0");
    }
}
